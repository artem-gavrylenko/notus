<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Employee extends \Core\Model
{
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('
          SELECT
            e.name,
            e.surname,
            d.type,
            p.name as position,
            GROUP_CONCAT(pe.permission SEPARATOR \', \') as permissions
          FROM employees e
          LEFT JOIN departaments d ON d.id = e.departament_id
          LEFT JOIN positions p ON p.id = e.position_id
          LEFT JOIN positions_permissions pp ON pp.position_id = e.position_id
          LEFT JOIN permissions pe ON pe.id = pp.permission_id
          GROUP BY e.id, pp.position_id
        ');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function create($data) {
        $db = static::getDB();

        $stmt = $db->prepare("
          INSERT INTO employees (name, surname, departament_id, position_id) 
          VALUES (:name, :surname, :departament_id, :position_id)
        ");

        $stmt->execute([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'departament_id' => $data['departament'],
            'position_id' => $data['position'],
        ]);
    }
}
