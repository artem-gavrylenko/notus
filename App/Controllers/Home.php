<?php

namespace App\Controllers;

use Core\View;
use Core\Controller;
use App\Models\Employee;
use App\Models\Departament;
use App\Models\Position;
use App\Services\RouteFactory;

class Home extends Controller
{
    public function indexAction()
    {
        $employees = Employee::getAll();
        $departaments = Departament::getAll();
        $positions = Position::getAll();

        View::renderTemplate('Home/index.html', [
            'employees' => $employees,
            'departaments' => $departaments,
            'positions' => $positions
        ]);
    }

    public function postAction()
    {
        Employee::create($_POST);

        RouteFactory::redirect('/');
    }
}
